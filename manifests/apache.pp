class riseuplabs::apache inherits apache::base {

  class { 'webhost': php => false }
  include site_passenger::package

  apache::config::global { 'default_server_name':
    content => 'ServerName dummy'
  }

  # this needs to be set because otherwise the _default_ ServerName is in
  # conflict with the fqdn of the machine, and then the default config
  # is loaded instead.
  apache::vhost::file {
    "${::hostname}.riseup.net":
      content => template("riseupsites/${::hostname}/${::hostname}.riseup.net");
  }

  apache::config::include { 'riseup_ssl_nocert.inc':
    source => 'puppet:///modules/riseupsites/riseup_ssl_nocert.inc'
  }

  apache::module {
    'userdir': ensure  => present;
  }

  File['default_apache_index'] {
    content => template('riseuplabs/index.erb')
  }

  file {
    '/var/www/robots.txt':
      owner  => www-data,
      group  => www-data,
      mode   => '0664',
      source => 'puppet:///modules/riseuplabs/robots.txt';

    '/etc/certs/https/otr.im.crt':
      content => hiera('otr.im_cert');

    '/etc/certs/https/otr.im.key':
      content => hiera('otr.im_key');
  }
}
