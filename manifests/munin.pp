class riseuplabs::munin {

  file {
    '/usr/local/share/munin-plugins/monkeysphere':
      source => "${::fileserver}/munin/monkeysphere",
      mode   => '0755',
      owner  => root,
      group  => root;
  }

  munin::plugin {
    'monkeysphere':
      ensure         => 'monkeysphere',
      script_path_in => '/usr/local/share/munin-plugins';
  }
}
