class riseuplabs::kgb {

  package { [ 'kgb-bot', 'kgb-client' ]:
    ensure => installed
  }

  file {
    '/etc/kgb-bot/kgb.conf':
      content => template('riseuplabs/kgb/kgb.conf.erb'),
      owner   => 'root',
      group   => 'Debian-kgb',
      mode    => '0640',
      require => Package['kgb-bot'];

    '/srv/kgb-client.conf':
      content => template('riseuplabs/kgb/kgb-client.conf.erb'),
      owner   => 'root',
      group   => 'gitolite',
      mode    => '0640',
      require => Package['kgb-client'];
  }
  augeas { 'enable-kgb':
    context => '/files/etc/default/kgb-bot/',
    changes => 'set BOT_ENABLED 1'
  }

  file { '/var/run/kgb-client':
    ensure => 'directory',
    owner  => 'gitolite',
    group  => 'gitolite'
  }

}
