class riseuplabs::redmine {

  file { '/var/cache/debconf/redmine.preseed':
    ensure  => present,
    owner   => root,
    group   => root,
    mode    => '0600',
    content => template('riseuplabs/redmine/redmine.preseed.erb'),
  }

  package { 'redmine':
    ensure       => installed,
    responsefile => '/var/cache/debconf/redmine.preseed',
    require      => File['/var/cache/debconf/redmine.preseed'],
  }

  package { [ 'libopenid-ruby', 'rubygems', 'redmine-mysql' ]:
    ensure => installed;
  }

  augeas {
    'logrotate_redmine':
      context => '/files/etc/logrotate.d/redmine/rule',
      changes => [ 'set file /var/log/redmine/*/*.log', 'set rotate 3',
                   'set schedule weekly', 'set compress compress',
                   'set missingok missingok', 'set ifempty notifempty',
                   'set copytruncate copytruncate' ]
  }

  mysql_database { 'redmine': ensure => present }

  mysql_user { 'redmine@localhost':
    ensure        => present,
    password_hash => trocla('mysql_redmine','mysql'),
    require       => Mysql_database['redmine'];
  }

  mysql_grant { 'redmine@localhost/redmine':
    privileges => [ 'select_priv', 'insert_priv', 'update_priv',
                    'delete_priv', 'create_priv', 'drop_priv',
                    'index_priv', 'alter_priv', 'Create_tmp_table_priv' ],
    require    => Mysql_user['redmine@localhost'];
  }

  file {
    # redmine configs
    '/etc/redmine/default/configuration.yml':
      source => 'puppet:///modules/riseuplabs/redmine/configuration.yml',
      mode   => '0640',
      owner  => www-data,
      group  => www-data,
      notify => Service['apache'];

    # redmine configs
    '/etc/redmine/otr/configuration.yml':
      source => 'puppet:///modules/riseuplabs/redmine/configuration.yml',
      mode   => '0640',
      owner  => www-data,
      group  => www-data,
      notify => Service['apache'];

    '/etc/redmine/default/database.yml':
      content => template('riseuplabs/redmine/database.yml.erb'),
      mode    => '0640',
      owner   => www-data,
      group   => www-data,
      notify  => Service['apache'];

    '/usr/share/redmine/public/favicon.ico':
      source => "${::fileserver}/riseup/favicon.ico",
      mode   => '0644',
      owner  => www-data,
      group  => www-data;

    # was renamed to configuration.yml since 1.2.0
    '/etc/redmine/default/email.yml':
      ensure => absent;

    '/usr/share/redmine/public/themes/Modula Mojito':
      ensure  => directory,
      recurse => true,
      source  => 'puppet:///modules/riseuplabs/redmine/theme';
  }

  # Compatibility for projects that were configured in the pre-Gitolite era
  file { '/srv/gitosis/repositories':
    ensure => link,
    target => '/var/lib/gitolite/repositories',
  }

  # note: home needs to be /home/www-data for the git-hosting plugin stuff in
  # .ssh
  user { 'www-data':
    ensure  => present,
    groups  => ['gitolite'],
    home    => '/home/www-data',
    shell   => '/bin/sh',
    system  => true,
    uid     => 33,
    gid     => 'www-data',
  }
}
