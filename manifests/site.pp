class riseuplabs::site {
  file {
    '/var/www/labs':
      ensure => directory,
      owner  => www-data, group => www-data, mode => '0775';
    '/var/www/labs/index.html':
      source => 'puppet:///modules/riseuplabs/index.html',
      owner  => www-data, group => www-data, mode => '0775';
    '/var/www/labs/jssl.ttf':
      source => 'puppet:///modules/riseuplabs/jssl.ttf',
      owner  => www-data, group => www-data, mode => '0775';
    '/var/www/labs/stylesheet.css':
      source => 'puppet:///modules/riseuplabs/stylesheet.css',
      owner  => www-data, group => www-data, mode => '0775';
    '/var/www/labs/images':
      source  => 'puppet:///modules/riseuplabs/images',
      recurse => true,
      owner   => www-data, group => www-data, mode => '0775';
  }

  apache::vhost::file {
    'riseuplabs':
      ensure  => present,
      content => template('riseuplabs/riseuplabs_vhost');
  }
}
