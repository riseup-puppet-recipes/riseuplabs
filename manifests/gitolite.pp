class riseuplabs::gitolite {

    class { '::gitolite':
    admin_key => 'ssh-rsa AAAAB3NzaC1yc2EAAAABIwAAAQEA1lHzO6il/4+V+KSiJZy34mxBKJNrn9Ah7VBxa3ss4AnahDgVwYqGlLk8xe45CShLRlu3nP4ccX06LUCJOBuLI2QZccR6+h2jfIEjXIaNC8lp12thIriEPUWZaKwV04fnhOnpA/VzNCRri8DOCuNvA8pfjg51DUtvhEuIV9UNxHCsLFSNg8RPngqNxrDgZJvjIMFFdTyOl0OdE1sN1zG2A6UPTlqO7Tmt6+/AByAS/C519nL0MIDix1S93sqaxaIE4kZSmVVx7Ft/albzWSIhIF/UbLxqfkc6L0HV0OpvBPMMuYUZkLC3DfAMggkxJsTTHkcR2Z1fIQ3P9am75WSwGw==',
    gituser   => gitolite,
    path      => '/var/lib/gitolite',
  }

  # Make SSHd aware of where the real Gitolite's authorized_keys live
  file {
    '/etc/ssh/authorized_keys/gitolite':
      ensure  => link,
      target  => '/var/lib/gitolite/.ssh/authorized_keys',
      require => File['/etc/ssh/authorized_keys'];

    '/var/lib/gitolite/.ssh/id_rsa':
      content => hiera('gitolite_private_key');
  }

  # make sure the user 'git' exists and is in the gitolite group
  # see #6678 for further discussion
  user { 'git':
    ensure  => present,
    groups  => ['gitolite'],
    require => [ Package[git], Package[gitolite] ],
    home    => '/var/lib/git',
    shell   => '/bin/bash',
    system  => true,
  }
}
