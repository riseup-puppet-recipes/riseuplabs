class riseuplabs inherits site_sshd {
  include riseuplabs::apache
  include riseuplabs::redmine
  include riseuplabs::gitolite
  include git::daemon
  include riseuplabs::munin
  include site_tor
  include riseuplabs::gitolite::hooks::common
  include site_shorewall
  include riseuplabs::kgb

  @@sshkey{ 'labs.riseup.net':
    ensure => present,
    type   => ssh-rsa,
    key    => $::sshrsakey,
  }

}

