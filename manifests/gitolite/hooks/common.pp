class riseuplabs::gitolite::hooks::common {

  file {

    # install a gl-post-init script that will remove gitolite's post-update.d symlink
    # and replace it with a directory, and populate it with the gitolite hooks
    # this is exactly what gitolite does itself, but we need to do this because
    # we want to put some other hooks in this directory in some repositories, but not
    # others
    '/var/lib/gitolite/.gitolite/hooks/common/gl-post-init':
      source => 'puppet:///modules/riseuplabs/gitolite/hooks/gl-post-init',
      mode   => '0744',
      owner  => gitolite,
      group  => gitolite;

    # riseup common hook directory we don't use the gitolite one, because when
    # gl-setup is run, all hooks in the gitolite common hook directory will be
    # installed in all repos
    '/var/lib/gitolite/.gitolite/hooks/riseup_common':
      ensure => directory,
      mode   => '0755',
      owner  => gitolite,
      group  => gitolite;

    # kgb bot for irc notifications
    '/var/lib/gitolite/.gitolite/hooks/riseup_common/kgb-post-receive.hook':
      source => 'puppet:///modules/riseuplabs/gitolite/hooks/kgb-post-receive.hook',
      mode   => '0700',
      owner  => gitolite,
      group  => gitolite;

    # github mirror push
    '/var/lib/gitolite/.gitolite/hooks/riseup_common/github_mirror-post-update.hook':
      source => 'puppet:///modules/riseuplabs/gitolite/hooks/github_mirror-post-update.hook',
      mode   => '0700',
      owner  => gitolite,
      group  => gitolite;

    '/var/lib/gitolite/.gitolite/hooks/riseup_common/email_notification-post-update.hook':
      ensure => link,
      target => '/usr/share/doc/git-core/contrib/hooks/post-receive-email';

    # hook to run everything in post-update.d/
    '/var/lib/gitolite/.gitolite/hooks/riseup_common/post_update.d.hook':
      source => 'puppet:///modules/riseuplabs/gitolite/hooks/post-update.d.hook',
      mode   => '0700',
      owner  => gitolite,
      group  => gitolite;

    # script to run periodically to make sure the hooks are in the right place
    '/usr/local/sbin/gitolite_hooks':
      source => 'puppet:///modules/riseuplabs/gitolite/install_hooks.sh',
      mode   => '0744',
      owner  => gitolite,
      group  => gitolite;
  }
}
