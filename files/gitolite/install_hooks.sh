#!/bin/bash
set -e

gitolite_root=/var/lib/gitolite
repository_root="${gitolite_root}/repositories"
hookdir="${gitolite_root}/.gitolite/hooks"
riseuphookdir="${hookdir}/riseup_common"

# verify both the post_update.d.hook and the post-receive hooks exist
if [ ! -e "${riseuphookdir}/post_update.d.hook" ]; then
    echo "${riseuphookdir}/post_update.d.hook does not exist, please investigate!"
    exit 2
fi

if [ ! -e "${hookdir}/common/post-receive" ]; then
    echo "${hookdir}/common/post-receive does not exist, please investigate!"
    exit 2
fi

/usr/bin/find "$repository_root" -mindepth 1 -maxdepth 1 -type d -not -path "/var/lib/gitolite/repositories/gitolite-admin.git" -not -path "./.gitolite/*" -print0 | while IFS= read -r -d '' repository
do

    # now we verify that each repository has the right hooks setup
    for action in receive update
    do
        # we need to make sure the post-${action} exists and is a symlink first
        if [[ -e "${repository}/hooks/post-${action}" && ! -h "${repository}/hooks/post-${action}" ]]; then
            echo "${repository}/hooks/post-${action} exists, but is not a symlink, please investigate!"
            exit 2
        fi
        
        # make sure the post-${action}.d directory is setup for every repository
        if [ ! -d "${repository}/hooks/post-${action}.d" ]; then
            mkdir "${repository}/hooks/post-${action}.d"
            chown gitolite:gitolite "${repository}/hooks/post-${action}.d"
        fi

    done

    # check that the hooks are pointing to the right place, each repository should have two hooks. 

    # First we check the post-receive, it should be post-receive -> ruby gitolite-redmine plugin hook
    if [ "$(readlink ${repository}/hooks/post-receive)" != "${hookdir}/common/post-receive" ]; then
        echo "unexpected ${repository}/hooks/post-receive link location, please investigate!"
        exit 2
    fi

    # then we check the post-update, it should be post-update -> our post_update.d.hook
    if [ "$(readlink -f ${repository}/hooks/post-update)" != "${riseuphookdir}/post_update.d.hook" ]; then
        echo "unexpected ${repository}/hooks/post-update link location, please investigate!"
        exit 2
    fi


    # now setup the shared-puppet module hooks
    case $(basename "$repository") in 
        shared-*|shared_*)
            if [ ! -h "${repository}/hooks/post-update" ]; then
                /bin/ln -s "${riseuphookdir}/post_update.d.hook" "${repository}/hooks/post-update"
            fi
            if [ ! -h "${repository}/hooks/post-receive.d/kgb-post-receive.hook" ]; then
                /bin/ln -s "${riseuphookdir}/kgb-post-receive.hook" "${repository}/hooks/post-receive.d/kgb-post-receive.hook"
            fi
            if [ ! -h "${repository}/hooks/post-update.d/github_mirror-post-update.hook" ]; then
                /bin/ln -s "${riseuphookdir}/github_mirror-post-update.hook" "${repository}/hooks/post-update.d/github_mirror-post-update.hook"
            fi
            ;;
        cg*|crabgrass*) if [ ! -h "${repository}/hooks/post-update" ]; then
                /bin/ln -s "${riseuphookdir}/post_update.d.hook" "${repository}/hooks/post-update"
            fi
            if [ ! -h "${repository}/hooks/post-update.d/github_mirror-post-update.hook" ]; then
                /bin/ln -s "${riseuphookdir}/github_mirror-post-update.hook" "${repository}/hooks/post-update.d/github_mirror-post-update.hook"
            fi
            ;;
        *)
            ;;
    esac
done
